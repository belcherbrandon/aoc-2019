import org.junit.jupiter.api.Test
import Utils.intArrayFromString
import Utils.intArrayFromFile
import Utils.mutableIntListFromFile
import org.assertj.core.api.Assertions

class TestDay2 {

    @Test
    fun runExample1() {
        Assertions
                .assertThat(Day2.runPart1(mutableListOf(1,9,10,3,2,3,11,0,99,30,40,50)))
                .isEqualTo(listOf(3500,9,10,70,2,3,11,0,99,30,40,50))

        Assertions
                .assertThat(Day2.runPart1(mutableListOf(1,0,0,0,99)))
                .isEqualTo(listOf(2,0,0,0,99))

        Assertions
                .assertThat(Day2.runPart1(mutableListOf(2,3,0,3,99)))
                .isEqualTo(listOf(2,3,0,6,99))

        Assertions
                .assertThat(Day2.runPart1(mutableListOf(2,4,4,5,99,0)))
                .isEqualTo(listOf(2,4,4,5,99,9801))

        Assertions
                .assertThat(Day2.runPart1(mutableListOf(1,1,1,4,99,5,6,0,99)))
                .isEqualTo(listOf(30,1,1,4,2,5,6,0,99))
    }

    @Test
    fun runPuzzle1() {
        val result = Day2.runPart1(mutableIntListFromFile("2-2", COMMA))
        println("Puzzle 2-1: $result")
        Assertions
                .assertThat(result[0])
                .isGreaterThan(337024)
                .isEqualTo(4484226)
    }

    @Test
    fun runPuzzle2() {
        (0..99).forEach {noun ->
            (0..99).forEach {verb ->
                val input = mutableIntListFromFile("2-2", COMMA)
                input[1] = noun
                input[2] = verb
                val result = Day2.runPart1(input)[0]
                if (result == 19690720) {
                    println("Puzzle 2-2: ${100 * noun + verb}")
                    return
                }
            }
        }
    }
}
