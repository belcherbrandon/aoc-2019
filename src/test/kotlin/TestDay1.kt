import org.junit.jupiter.api.Test
import Utils.intArrayFromString
import Utils.intArrayFromFile
import org.assertj.core.api.Assertions

class TestDay1 {

    @Test
    fun runExample1() {
        Assertions
                .assertThat(Day1.runPart1(intArrayOf(12)))
                .isEqualTo(2)

        Assertions
                .assertThat(Day1.runPart1(intArrayOf(14)))
                .isEqualTo(2)

        Assertions
                .assertThat(Day1.runPart1(intArrayOf(1969)))
                .isEqualTo(654)

        Assertions
                .assertThat(Day1.runPart1(intArrayOf(100756)))
                .isEqualTo(33583)
    }

    @Test
    fun runPuzzle1() {
        val result = Day1.runPart1(intArrayFromFile("1-1", LINE))
        println("Puzzle 1-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(3126794)
    }

    @Test
    fun runExample2() {
        Assertions
                .assertThat(Day1.runPart2(intArrayOf(14)))
                .isEqualTo(2)

        Assertions
                .assertThat(Day1.runPart2(intArrayOf(1969)))
                .isEqualTo(966)

        Assertions
                .assertThat(Day1.runPart2(intArrayOf(100756)))
                .isEqualTo(50346)
    }

    @Test
    fun runPuzzle2() {
        val result = Day1.runPart2(intArrayFromFile("1-1", LINE))
        println("Puzzle 1-2: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(4687331)
    }
}
