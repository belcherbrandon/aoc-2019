import kotlin.math.floor

object Day2 {

    fun runPart1(input: MutableList<Int>): MutableList<Int> {
        input.windowed(4, 4) {
            when {
                it[0] == 1 -> {
                    input[it[3]] = input[it[1]] + input[it[2]]
                }
                it[0] == 2 -> {
                    input[it[3]] = input[it[1]] * input[it[2]]
                }
                else -> {
                    return@windowed
                }
            }
        }
        return input
    }
}