import kotlin.math.floor

object Day1 {

    fun runPart1(input: IntArray): Int {
        return input.map { calculateFuel(it) }.sum()
    }

    fun runPart2(input: IntArray): Int {
        return input.map {
            calculateFuelRecursive(0, it)
        }.sum()
    }

    private fun calculateFuel(mass: Int) = (floor(mass.toDouble() / 3) - 2).toInt()

    private fun calculateFuelRecursive(totalMass: Int, currentMass: Int): Int {
        val fuelCost = calculateFuel(currentMass)
        return if (fuelCost <= 0) {
            0
        } else {
            totalMass + fuelCost + calculateFuelRecursive(totalMass, fuelCost)
        }
    }
}